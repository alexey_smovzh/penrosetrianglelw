package com.fasthamster.penrosetrianglelw;

import com.badlogic.gdx.graphics.g3d.ModelInstance;

/**
 * Created by alex on 22.02.16.
 */
public class Cube {
    public ModelInstance instance;
    public Position position;

    public Cube(ModelInstance instance, Position position) {
        this.instance = instance;
        this.position = position;
    }
}
