package com.fasthamster.penrosetrianglelw;

import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.graphics.g3d.model.Node;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;

/**
 * Created by alex on 22.02.16.
 */
public class Position {

    public Vector3 position;
    public byte target;
    public Vector3 step;
    public int count;
    public Vector2 angle;

    public static final float TRIANGLE_SIDE = 2f;           // MEGA constant!!
    private static final int STEPS = (int)TRIANGLE_SIDE * 50;
    private static final float DEPTH = 0.025f;


    private static final byte POINT_0_ALIAS = 0;
    private static final byte POINT_1_ALIAS = 1;
    private static final byte POINT_2_ALIAS = 2;

    /*      2
     *      |\
     *      | 1
     *      |/
     *      0
     */

    private static final Vector3 POINT_0 = new Vector3(0f, 0f, 0f);
    private static final Vector3 POINT_1 = new Vector3(0.866f * TRIANGLE_SIDE, TRIANGLE_SIDE / 2, 0f);  // (Math.sqrt(3) / 2f) * 3f;    // 0.866 * 3f;
    private static final Vector3 POINT_2 = new Vector3(0f, TRIANGLE_SIDE, 0f);

    private static final Vector3 STEP_0_1 = new Vector3((0.866f * TRIANGLE_SIDE) / STEPS, (TRIANGLE_SIDE  / 2) / STEPS, DEPTH / STEPS);
    private static final Vector3 STEP_1_2 = new Vector3(-(0.866f * TRIANGLE_SIDE) / STEPS, (TRIANGLE_SIDE  / 2) / STEPS, DEPTH / STEPS);
    private static final Vector3 STEP_2_0 = new Vector3(0f, -(TRIANGLE_SIDE / STEPS), DEPTH / STEPS);

    // Pre-calculated equations
    public static final float BISECT = 0.866f * TRIANGLE_SIDE;
    private static final float THIRD_BISECT = BISECT / 3;
    private static final float HALF_SIDE = TRIANGLE_SIDE / 2;
    private static final float THIRD_SIDE = TRIANGLE_SIDE / 3;
    private static final float ONE_SIX_SIDE = TRIANGLE_SIDE / 6;


    public Position(Vector3 position, byte target, Vector3 step, int count, Vector2 angle) {
        this.position = position;
        this.target = target;
        this.step = step;
        this.count = count;
        this.angle = angle;
    }

    // Initial cubes positions and params
    public static final Position[] positions = new Position[] {
            new Position(POINT_0, POINT_1_ALIAS, STEP_0_1, STEPS, new Vector2(-1f, 0f)),          // Point0, target Point1
            new Position(new Vector3(THIRD_BISECT, ONE_SIX_SIDE, DEPTH / 3), POINT_1_ALIAS, STEP_0_1, (STEPS / 3) * 2, new Vector2(-1f, 0f)),
            new Position(new Vector3(THIRD_BISECT * 2f, ONE_SIX_SIDE * 2f, (DEPTH / 3) * 2), POINT_1_ALIAS, STEP_0_1, STEPS / 3, new Vector2(-1f, 0f)),

            new Position(POINT_1, POINT_2_ALIAS, STEP_1_2, STEPS, new Vector2(1f, 0f)),          // Point1, target Point2
            new Position(new Vector3(THIRD_BISECT * 2, ONE_SIX_SIDE + HALF_SIDE, DEPTH / 3), POINT_2_ALIAS, STEP_1_2, (STEPS / 3) * 2, new Vector2(1f, 0f)),
            new Position(new Vector3(THIRD_BISECT, (ONE_SIX_SIDE * 2f) + HALF_SIDE, (DEPTH / 3) * 2), POINT_2_ALIAS, STEP_1_2, STEPS / 3, new Vector2(1f, 0f)),

            new Position(POINT_2, POINT_0_ALIAS, STEP_2_0, STEPS, new Vector2(0f, -1f)),          // Point2, target Point0
            new Position(new Vector3(0f, THIRD_SIDE * 2f, DEPTH / 3), POINT_0_ALIAS, STEP_2_0, (STEPS / 3) * 2, new Vector2(0f, -1f)),
            new Position(new Vector3(0f, THIRD_SIDE, (DEPTH / 3) * 2), POINT_0_ALIAS, STEP_2_0, STEPS / 3, new Vector2(0f, -1f))
    };

    public void move(ModelInstance instance) {

        position.add(step);
        translateRotate(instance, position, angle);
        count--;
    }

    public static void translateRotate(ModelInstance i, Vector3 pos, Vector2 angle) {

        Node node = i.nodes.get(0);
        node.translation.set(pos);
        node.rotation.setEulerAngles(angle.x, angle.y, 0f);
        i.calculateTransforms();
    }

    public boolean reachTarget() {
        return count == 0 ? true : false;
    }

    public void setMoveVars(byte target, Vector3 step, int count) {
        this.target = target;
        this.step = step;
        this.count = count;
    }

    public void setNewTarget() {

        switch (target) {
            case POINT_0_ALIAS:         // we reach point 0, next point 1
                setMoveVars(POINT_1_ALIAS, STEP_0_1, STEPS);
                position.sub(0f, 0f, DEPTH);
                angle = new Vector2(-1f, 0f);
                break;

            case POINT_1_ALIAS:
                setMoveVars(POINT_2_ALIAS, STEP_1_2, STEPS);
                position.sub(0f, 0f, DEPTH);
                angle = new Vector2(1f, 0f);
                break;

            case POINT_2_ALIAS:
                setMoveVars(POINT_0_ALIAS, STEP_2_0, STEPS);
                position.sub(0f, 0f, DEPTH);
                angle = new Vector2(0f, -1f);
                break;
        }
    }
}

