package com.fasthamster.penrosetrianglelw;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.ModelBatch;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.graphics.g3d.attributes.BlendingAttribute;
import com.badlogic.gdx.graphics.g3d.attributes.FloatAttribute;
import com.badlogic.gdx.graphics.g3d.loader.G3dModelLoader;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.UBJsonReader;

// TODO: change cubes style to material design (make style choice?)
// TODO: custom shader, because default shader uses 25% of cpu
// TODO: changing speed via changing steps, not via changing fps

public class PenroseTriangleLW extends ApplicationAdapter {

    public static final String PREFS_NAME = "settings";
    public static final String SPEED_ALIAS = "speed";
    public static final String COLOR_ALIAS = "color";
    public static final int SPEED_DEFAULT = 30;
    public static final int COLOR_DEFAULT = (0xFF << 24) | (71 << 16) | (92 << 8) | 104;     // 41, 59, 84


    private Array<Cube> cubes = new Array<Cube>();
    private Model model;
    private OrthographicCamera camera;
    private ModelBatch batch;
    private boolean paused;

    private static int speed;
    private static float r, g, b;


    @Override
	public void create () {

        setupModel();

        // Load preferences
        Preferences preferences = Gdx.app.getPreferences(PREFS_NAME);
        setSpeed(preferences.getInteger(SPEED_ALIAS, SPEED_DEFAULT));
        setColors(preferences.getInteger(COLOR_ALIAS, COLOR_DEFAULT));

		batch = new ModelBatch();

        paused = false;

	}

    private void setupModel() {

        UBJsonReader jsonReader = new UBJsonReader();
        G3dModelLoader modelLoader = new G3dModelLoader(jsonReader);

        model = modelLoader.loadModel(Gdx.files.internal("cubes.g3db"));

        int idx = 0;
        for(int i = 0; i < Position.positions.length; i++) {

            if(idx >= model.nodes.size) idx = 0;

            ModelInstance m = new ModelInstance(model, model.nodes.get(idx).id);
            idx++;

            m.materials.get(0).set(new BlendingAttribute(GL20.GL_BLEND));
            m.materials.get(0).set(new BlendingAttribute(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA));
            m.materials.get(0).set(new FloatAttribute(FloatAttribute.AlphaTest, 0.1f));

            Position.translateRotate(m, Position.positions[i].position, Position.positions[i].angle);
            Cube c = new Cube(m, Position.positions[i]);

            cubes.add(c);
        }
    }

    // Setters
    public static void setSpeed(int s) {
        PenroseTriangleLW.speed = s;
    }

    public static void setColors(int color) {
        PenroseTriangleLW.r = (float)(color >>> 16 & 0xFF) / 255;
        PenroseTriangleLW.g = (float)(color >>> 8 & 0xFF) / 255;
        PenroseTriangleLW.b = (float)(color & 0xFF) / 255;
    }

    @Override
	public void render () {

        if(!paused) {

            Gdx.gl.glViewport(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
            Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);
            Gdx.gl.glClearColor(r, g, b, 1f);

            if(speed != 0) {
                batch.begin(camera);                                         // Render cycle

                for (Cube c : cubes) {
                    batch.render(c.instance);
                    if (c.position.reachTarget()) {                          // Update cubes to new target
                        c.position.setNewTarget();
                    } else {
                        c.position.move(c.instance);
                    }
                }

                batch.end();

                sleep(speed);

            } else {                                                         // if speed == 0, stop moving
                batch.begin(camera);                                         // Render cycle

                for (Cube c : cubes) {
                    batch.render(c.instance);
                }

                batch.end();
            }
        }
	}

    // Sleep for power consumption
    private long diff, start = System.currentTimeMillis();
    private void sleep(int fps) {

        diff = System.currentTimeMillis() - start;
        long targetDelay = 1000 / fps;
        if(diff < targetDelay) {
            try {
                Thread.sleep(targetDelay - diff);
            } catch (InterruptedException e) {	}
        }
        start = System.currentTimeMillis();
    }

    @Override
    public void resize(int w, int h) {

        float aspect = (float) w / (float) h;

        if(w < h) {                 // Vertical
            float size = Position.TRIANGLE_SIDE * 3f;
            camera = new OrthographicCamera(size * aspect, size);
            camera.position.set(Position.TRIANGLE_SIDE * aspect * 0.5f, Position.TRIANGLE_SIDE * 0.5f, 4f);
        } else {
            float size = Position.TRIANGLE_SIDE * 2f;
            camera = new OrthographicCamera(size * aspect, size);
            camera.position.set(Position.TRIANGLE_SIDE * 0.4f, Position.TRIANGLE_SIDE * 0.5f, 4f);
        }

        camera.update();
    }

    @Override
    public void pause() {
        paused = true;
    }

    @Override
    public void resume() {
        paused = false;
    }

    @Override
    public void dispose () {

        cubes.clear();
        batch.dispose();
        model.dispose();

    }
}
