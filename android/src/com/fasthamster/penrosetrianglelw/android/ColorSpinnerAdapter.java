package com.fasthamster.penrosetrianglelw.android;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;

/**
 * Created by alex on 11.03.16.
 */

public class ColorSpinnerAdapter extends BaseAdapter {

    private Context context;
    private LayoutInflater inflater;

    public static final int[] colors = {
            Color.rgb(63, 63, 63),      // grey
            Color.rgb(107, 107, 107),
            Color.rgb(71, 92, 104),

            Color.rgb(12, 129, 162),       // blue
            Color.rgb(17, 103, 152),
            Color.rgb(9, 77, 140),
            Color.rgb(35, 58, 128),
            Color.rgb(45, 42, 123),

            Color.rgb(81, 42, 123),      // purple - red
            Color.rgb(138, 40, 141),
            Color.rgb(162, 32, 106),
            Color.rgb(111, 14, 18),
            Color.rgb(150, 20, 26),
            Color.rgb(179, 29, 36),

            Color.rgb(204, 81, 39),    // orange - yellow
            Color.rgb(205, 99, 41),
            Color.rgb(206, 119, 42),
            Color.rgb(205, 141, 43),
            Color.rgb(204, 162, 42),
            Color.rgb(205, 183, 46),

            Color.rgb(204, 205, 42),    // green
            Color.rgb(154, 182, 59),
            Color.rgb(112, 162, 65),
            Color.rgb(58, 153, 69),
            Color.rgb(69, 100, 38),
            Color.rgb(47, 70, 24),
    };

    public ColorSpinnerAdapter(Context context) {
        this.context = context;

        this.inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public int getItemPosition(int item) {

        int result = 0;

        for(int i = 0; i < colors.length; i++) {
            if(colors[i] == item)
                result = i;
        }

        return result;
    }

    @Override
    public int getCount() {
        return colors.length;
    }

    @Override
    public Object getItem(int position) {
        return colors[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View row = inflater.inflate(R.layout.color_item, parent, false);
        RelativeLayout layout = (RelativeLayout)row.findViewById(R.id.rl_color_item);
        layout.setBackgroundColor(colors[position]);

        return row;
    }
}

