package com.fasthamster.penrosetrianglelw.android;

import android.content.Context;
import android.content.SharedPreferences;

import com.fasthamster.penrosetrianglelw.PenroseTriangleLW;

/**
 * Created by alex on 26.02.16.
 */

public class SettingsHandler {

    Context context;
    SharedPreferences settings;
    SharedPreferences.Editor editor;

    public int speed;
    private int color;


    public SettingsHandler(Context context) {
        this.context = context;
        load();
    }

    public void load() {

        settings = context.getSharedPreferences(PenroseTriangleLW.PREFS_NAME, 0);
        speed = settings.getInt(PenroseTriangleLW.SPEED_ALIAS, PenroseTriangleLW.SPEED_DEFAULT);
        color = settings.getInt(PenroseTriangleLW.COLOR_ALIAS, PenroseTriangleLW.COLOR_DEFAULT);

    }

    public void save() {

        editor = settings.edit();
        editor.putInt(PenroseTriangleLW.SPEED_ALIAS, speed);
        editor.putInt(PenroseTriangleLW.COLOR_ALIAS, color);
        editor.commit();

    }

    public int getSpeed() {
        return speed;
    }

    public int getColor() {
        return color;
    }

    public void setSpeed(int s) {
        this.speed = s;
    }

    public void setColor(int c) {
        this.color = c;
    }
}
