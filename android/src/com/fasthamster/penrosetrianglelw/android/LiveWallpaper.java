package com.fasthamster.penrosetrianglelw.android;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;
import com.badlogic.gdx.backends.android.AndroidLiveWallpaperService;
import com.badlogic.gdx.backends.android.AndroidWallpaperListener;
import com.fasthamster.penrosetrianglelw.PenroseTriangleLW;

/*
 * Created by alex on 26.02.16.
 */
public class LiveWallpaper extends AndroidLiveWallpaperService {

    public static SettingsHandler settings;


    @Override
    public void onCreateApplication() {

        super.onCreateApplication();

        settings = new SettingsHandler(getApplicationContext());

        AndroidApplicationConfiguration config = new AndroidApplicationConfiguration();
        config.useCompass = false;
        config.useWakelock = false;
        config.useAccelerometer = false;
        config.getTouchEventsForLiveWallpaper = false;

        ApplicationListener listener = new PenroseTriangleLWListener();

        initialize(listener, config);

    }

    @Override
    public void onDestroy() {
        super.onDestroy();

    }

    public static class PenroseTriangleLWListener extends PenroseTriangleLW implements AndroidWallpaperListener {


        @Override
        public void offsetChange(float xOffset, float yOffset, float xOffsetStep, float yOffsetStep, int xPixelOffset, int yPixelOffset) {

        }

        @Override
        public void previewStateChange(boolean isPreview) {

        }
    }
}
