package com.fasthamster.penrosetrianglelw.android;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.fasthamster.penrosetrianglelw.PenroseTriangleLW;

/**
 * Created by alex on 26.02.16.
 */
public class SettingsActivity extends Activity implements SeekBar.OnSeekBarChangeListener,
                                                          AdapterView.OnItemSelectedListener {

    SeekBar seekBar;
    TextView result;
    Spinner color;

    private ColorSpinnerAdapter adapter;
    private int oldColorPosition;


    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        // Layouts
        setContentView(R.layout.settings);
        seekBar = (SeekBar)findViewById(R.id.seek_bar);
        result = (TextView)findViewById(R.id.tv_result);
        color = (Spinner)findViewById(R.id.sp_color);

        // Adapters
        adapter = new ColorSpinnerAdapter(SettingsActivity.this);
        color.setAdapter(adapter);

        // Listener
        color.setOnItemSelectedListener(this);
        seekBar.setOnSeekBarChangeListener(this);

        // States
        seekBar.setProgress(LiveWallpaper.settings.getSpeed());
        color.setSelection(adapter.getItemPosition(LiveWallpaper.settings.getColor()));

    }

    @Override
    protected void onStop() {

        super.onStop();

        PenroseTriangleLW.setSpeed(LiveWallpaper.settings.getSpeed());
        PenroseTriangleLW.setColors(LiveWallpaper.settings.getColor());
        LiveWallpaper.settings.save();

    }

    @Override
    protected void onDestroy() {

        super.onDestroy();

    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

        LiveWallpaper.settings.setSpeed(progress);
        result.setText(Integer.toString(progress));

    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        LiveWallpaper.settings.setColor(ColorSpinnerAdapter.colors[position]);
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
